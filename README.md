Grafana in docker role
=========

Installs Grafana on Debian/Ubuntu servers.

Dependencies
------------

`tyumentsev4.docker` role to install docker engine.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.grafana
```
